$(document).ready( function ()
{
    var msg = $('#msg').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data berhasil disimpan.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data berhasil diubah.');

    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    var userTable = $('#userTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/account-management/user/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = userTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:true,orderable:true},
            {data: 'nik', name: 'nik',searchable:true,visible:true,orderable:true},
            {data: 'name', name: 'name',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ]
    });

    var dtable = $('#userTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#form').submit(function (event){
        event.preventDefault();
        var name = $('#name').val();
        // var select_kelamin = $('#select_kelamin').val();

        if(!name)
        {
            $("#alert_warning").trigger("click", 'Nama wajib diisi');
            return false;
        }

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
					data:new FormData($("#form")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        document.location.href = '/account-management/user';
                    },
                    error: function (response) {
                        $.unblockUI();

                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            }
        });
    });

    $('#warehouse').trigger('change');
    mappings = JSON.parse($('#mappings').val());
    var form_status = $('#form_status').val();

    if(form_status == 'create')
    {
        render();
    }else
    {
        var url_data_role = $('#url_data_role').val();
        var chacked_is_super_admin = $('#chacked_is_super_admin').val();

        if(chacked_is_super_admin) $("#is_super_admin").parent().find(".switchery").prop('checked', true).trigger("click");
        else $("#is_super_admin").parent().find(".switchery").prop('checked', false).trigger("click");

        $('#roleTable').DataTable().destroy();
        $('#roleTable tbody').empty();
        var table = $('#roleTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            scroller:true,
            destroy:true,
            deferRender:true,
            bFilter:true,
            ajax: {
                type: 'GET',
                url: url_data_role,
            },
            fnCreatedRow: function (row, data, index) {
                var info = table.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'display_name', name: 'display_name',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });

        var dtable2 = $('#roleTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable2.search(this.value).draw();
                }
                if (this.value == "") {
                    dtable2.search("").draw();
                }
                return;
        });
        dtable2.draw();

        render();
    }
});

$('#submit_button').on('click',function(){
    $('#form').trigger('submit');
});

$('#warehouse').on('change',function()
{
    var value = $(this).val();
    $.ajax({
        type: 'get',
        url: '/account-management/user/get-absence?warehouse_id='+value
    })
    .done(function(response){
        $('#auto_completes').val(JSON.stringify(response)).trigger('change');
    })

});

$('#auto_completes').on('change',function()
{
    available_nik = JSON.parse($('#auto_completes').val());

    $("#nik").autocomplete({
        source: available_nik
    });
});

$('#nik').on('change',function()
{
    var nik       = $(this).val();
    var warehouse = $('#warehouse').val();

    if(nik && warehouse)
    {
        var value     = nik.split(':');
        $('#nik').val(value[0]);
        $('#name').val(value[1]);
    }else
    {
        $('#name').val('');
    }

});


$('#select_role').on('change',function()
{
    var value       = $(this).val();
    var name        = $(this).select2('data')[0].text;
    var form_status = $('#form_status').val();

    if(value)
    {
        var input = {
            'id': value,
            'name': name
        };

        var diff = checkItem(value);
        if (!diff)
        {
            $("#alert_warning").trigger("click", name+' sudah di dipilih.');
            $(this).val('').trigger('change');
            return false;
        }

        if(form_status == 'edit')
        {
            var user_id = $('#user_id').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '/account-management/user/store/role',
                data: {
                    user_id: user_id,
                    role_id: value
                }
            })
            .done(function () {
                $('#roleTable').DataTable().ajax.reload();
            });
        }

        if (name) mappings.push(input);

        render();
        $(this).val('').trigger('change');
    }



});

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $('#userTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data berhasil hapus');
    });
}

function reset(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $('#userTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Password berhasil direset');
    });
}

function hapusModal(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        beforeSend: function () {
            mappings = [];
        },
        success: function (response) {
            var permissions = response;
            for (idx in permissions)
            {
                var data = permissions[idx];
                var input = {
                    'id': data.id,
                    'name': data.display_name
                };
                mappings.push(input);
            }


        }
    })
    .done(function () {
        $('#roleTable').DataTable().ajax.reload();
        render();
    });
}

function render()
{
    getIndex();
    $('#mappings').val(JSON.stringify(mappings));

    var tmpl = $('#role_table').html();
    Mustache.parse(tmpl);
    var data = { item: mappings };
    var html = Mustache.render(tmpl, data);
    $('#user_role').html(html);
    bind();
}

function bind()
{
    $('.btn-delete-item').on('click', deleteItem);
}

function getIndex()
{
    for (idx in mappings)
    {
        mappings[idx]['_id'] = idx;
        mappings[idx]['no'] = parseInt(idx) + 1;
    }
}

function deleteItem()
{
    var i = $(this).data('id');

    mappings.splice(i, 1);
    render();
}

function checkItem(id)
{
    for (var i in mappings)
    {
        var data = mappings[i];

        if (data.id == id)
            return false;
    }

    return true;
}

function validateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


$(document).ready(function () {

        load_bulan();
        // $('#select_bulan').on('change',function(){

        //     var bulan = $(this).val();
        //     $('#_bulan').val(bulan);

        console.log($('#select_bulan').val());
        // });

        $('#lock_faktur').submit(function (event){
            event.preventDefault();
            var bulan   = $('#select_bulan').val();

            console.log(bulan);
            if (!bulan) {
                $("#alert_warning").trigger("click", 'Please choose bulan first');
                return false;
            }

            bootbox.confirm("Are you sure want to save this data ?.", function (result) {
                if(result){
                    $.ajax({
                        type: "POST",
                        url: $('#lock_faktur').attr('action'),
                        data: $('#lock_faktur').serialize(),
                        beforeSend: function () {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function (result) {
                            $.unblockUI();
                            $("#alert_success").trigger("click", result.message);
                            $('#lock_faktur').trigger("reset");
                            $("#select_bulan").empty();
                            load_bulan();
                        },
                        error: function (response) {
                            $.unblockUI();

                            if (response.status == 422){
                                $("#alert_warning").trigger("click", response.responseJSON.message);
                                $('#lock_faktur').trigger("reset");
                                $("#select_bulan").empty();
                                load_bulan();
                            }
                        }
                    });
                }
            });

        });


});

function load_bulan() {
    $("#select_bulan").empty();
    $("#select_bulan").append('<option value="">-- Pilih Bulan --</option>');
    $("#select_bulan").append('<option value="1">JANUARI</option>');
    $("#select_bulan").append('<option value="2">FEBRUARI</option>');
    $("#select_bulan").append('<option value="3">MARET</option>');
    $("#select_bulan").append('<option value="4">APRIL</option>');
    $("#select_bulan").append('<option value="5">MEI</option>');
    $("#select_bulan").append('<option value="6">JUNI</option>');
    $("#select_bulan").append('<option value="7">JULI</option>');
    $("#select_bulan").append('<option value="8">AGUSTUS</option>');
    $("#select_bulan").append('<option value="9">SEPTEMBER</option>');
    $("#select_bulan").append('<option value="10">OKTOBER</option>');
    $("#select_bulan").append('<option value="11">NOVEMBER</option>');
    $("#select_bulan").append('<option value="12">DESEMBER</option>');
}

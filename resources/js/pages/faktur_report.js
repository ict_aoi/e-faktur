$(document).ready(function () {


        $('input.input-date').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            todayHighlight: true
        });

        var fakturReport =  $('#report_faktur_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/faktur/data-report',
                data: function(d) {
                    return $.extend({}, d, {
                        "start_date": $('#start_date').val(),
                        "end_date"  : $('#end_date').val(),
                        "factory"   : $('#select_factory').val(),
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = fakturReport.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
                $('td', row).eq(7).css('min-width', '120px');
                $('td', row).eq(9).css('min-width', '200px');
                // $('td', row).eq(10).css('min-width', '200px');
                $('td', row).eq(15).css('min-width', '200px');
            },
            columns: [
                {data: 'uuid', name: 'id',searchable:true,visible:false,orderable:false},
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'fm', name: 'fm',searchable:false,visible:true,orderable:false},
                {data: 'kode_jenis', name: 'kode_jenis',searchable:false,visible:true,orderable:true},
                {data: 'fg_pengganti', name: 'fg_pengganti',searchable:true,visible:true,orderable:true},
                {data: 'nomor_faktur', name: 'nomor_faktur',searchable:true,visible:true,orderable:true},
                {data: 'bulan_finance', name: 'bulan_finance',searchable:true,visible:true,orderable:true},
                {data: 'tahun_pajak', name: 'tahun_pajak',searchable:true,visible:true,orderable:true},
                {data: 'tanggal_faktur', name: 'tanggal_faktur',searchable:true,visible:true,orderable:true},
                {data: 'npwp_penjual', name: 'npwp_penjual',searchable:true,visible:true,orderable:true},
                {data: 'nama_penjual', name: 'nama_penjual',searchable:true,visible:true,orderable:true},
                // {data: 'alamat_penjual', name: 'alamat_penjual',searchable:false,visible:true,orderable:false},
                {data: 'jumlah_dpp', name: 'jumlah_dpp',searchable:false,visible:true,orderable:true},
                {data: 'jumlah_ppn', name: 'jumlah_ppn',searchable:false,visible:true,orderable:true},
                {data: 'jumlah_ppnbm', name: 'jumlah_ppnbm',searchable:false,visible:true,orderable:false},
                {data: 'creditable', name: 'creditable',searchable:false,visible:true,orderable:false},
                {data: 'referensi', name: 'referensi',searchable:false,visible:true,orderable:false},
            ]
        });

        var dtable = $('#report_faktur_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();

        $('#select_factory').on('change',function()
        {
            var factory = $(this).val();
            $('#_factory').val(factory);
            dtable.draw();
        });

        $('#start_date').on('change',function(){

            var start_date = $(this).val();
            $('#_start_date').val(start_date);
            console.log(start_date);
            dtable.draw();
        });

        $('#end_date').on('change',function(){

            var end_date = $(this).val();
            $('#_end_date').val(end_date);
            console.log(end_date);
            dtable.draw();
        });

        $('#export_report').click(function(){

            var start_date = $('#start_date').val();
            var end_date   = $('#end_date').val();
            var factory    = $('#select_factory').val();

            if (!start_date) {
                $("#alert_warning").trigger("click", 'Please choose start date first');
                return false;
            }
            if (!end_date) {
                $("#alert_warning").trigger("click", 'Please choose end date first');
                return false;
            }
            if (!factory) {
                $("#alert_warning").trigger("click", 'Please choose factory first');
                return false;
            }
        });

        // $('#export_report').submit(function (event) {
        //     event.preventDefault();
            // var bulan   = $('#select_bulan').val();
            // var factory = $('#select_factory').val();

            // if (!bulan) {
            //     $("#alert_warning").trigger("click", 'Please choose bulan first');
            //     return false;
            // }
            // if (!factory) {
            //     $("#alert_warning").trigger("click", 'Please choose factory first');
            //     return false;
            // }

        //     $.ajax({
        //         type: "GET",
        //         url: $('#export_report').attr('action'),
        //         data: $('#export_report').serialize(),
        //         beforeSend: function () {
        //             $.blockUI({
        //                 message: '<i class="icon-spinner4 spinner"></i>',
        //                 overlayCSS: {
        //                     backgroundColor: '#fff',
        //                     opacity: 0.8,
        //                     cursor: 'wait'
        //                 },
        //                 css: {
        //                     border: 0,
        //                     padding: 0,
        //                     backgroundColor: 'transparent'
        //                 }
        //             });
        //         },
        //         complete: function () {
        //             $.unblockUI();
        //         },
        //         success: function () {
        //             $('#export_report').trigger("reset");
        //             // $('#select_issue').val('');
        //             // req_load_list_issue();
        //             $("#select_bulan").empty();
        //             $("#select_factory").empty();
        //             load_bulan();
        //             load_factory();
        //             $('#report_faktur_table').DataTable().ajax.reload();
        //             $("#alert_success").trigger("click", 'Report Berhasil!');
        //             // document.location.href = '/account-management/permission';
        //         },
        //         error: function (response) {
        //             $.unblockUI();

        //             if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
        //         }

        //     });

        // });

        function load_bulan() {
            $("#select_bulan").empty();
            $("#select_bulan").append('<option value="">-- Pilih Bulan --</option>');
            $("#select_bulan").append('<option value="1">JANUARI</option>');
            $("#select_bulan").append('<option value="2">FEBRUARI</option>');
            $("#select_bulan").append('<option value="3">MARET</option>');
            $("#select_bulan").append('<option value="4">APRIL</option>');
            $("#select_bulan").append('<option value="5">MEI</option>');
            $("#select_bulan").append('<option value="6">JUNI</option>');
            $("#select_bulan").append('<option value="7">JULI</option>');
            $("#select_bulan").append('<option value="8">AGUSTUS</option>');
            $("#select_bulan").append('<option value="9">SEPTEMBER</option>');
            $("#select_bulan").append('<option value="10">OKTOBER</option>');
            $("#select_bulan").append('<option value="11">NOVEMBER</option>');
            $("#select_bulan").append('<option value="12">DESEMBER</option>');
        }

        function load_factory() {
            $("#select_factory").empty();
            $("#select_factory").append('<option value="">-- Pilih Factory --</option>');
            $("#select_factory").append('<option value="031233745503000">AOI 1</option>');
            $("#select_factory").append('<option value="031233745503001">AOI 2</option>');
            $("#select_factory").append('<option value="013459292503001">BBIS</option>');
        }
});

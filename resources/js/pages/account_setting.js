$(document).ready( function ()
{
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    $('#form').submit(function (event)
    {
        event.preventDefault();
        var name    = $('#name').val();
        var email   = $('#email').val();
        
        if(!name)
        {
            $("#alert_warning").trigger("click", 'Please insert name first');
            return false;
        }

        if(!email)
        {
            $("#alert_warning").trigger("click", 'Please insert email first');
            return false;
        }


        if(!validateEmail(email))
        {
            $("#alert_warning").trigger("click", 'Format email is wrong');
            return false;
        }

        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    cache:false,
                    url: $('#form').attr('action'),
                    data:new FormData($("#form")[0]),
                    contentType: false,
                    processData:false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click", 'Data successfully updated.');
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    }
                });
            }
        });
    });
});

function validateEmail(email) 
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
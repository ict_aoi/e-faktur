$(document).ready(function () {
    // var msg = $('#msg').val();

    // load_bulan();
    fakturTable();
    // if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    // else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

    $('#insert_faktur').submit(function (event) {
        event.preventDefault();
        var bulan = $('#select_bulan').val();
        var url_pajak = $('#url_pajak').val();

        // console.log(bulan);
        // console.log(url_pajak);

        if (!bulan) {
            $("#alert_warning").trigger("click", 'Please insert bulan first');
            return false;
        }
        if (!url_pajak) {
            $("#alert_warning").trigger("click", 'Please scan first');
            return false;
        }

        $.ajax({
            type: "POST",
            url: $('#insert_faktur').attr('action'),
            data: $('#insert_faktur').serialize(),
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function () {

                        // var faktur = response.faktur_id;
                        // $('#insertPermissionModal').modal('hide');
                        // var bulan = $('#select_bulan').val();

                        $('#insert_faktur').trigger("reset");
                        $('#select_bulan').val(bulan);
                        // req_load_list_issue();
                        // $("#select_bulan").empty();
                        // load_bulan();
                        $('#fakturTable').DataTable().ajax.reload();
                        $("#alert_success").trigger("click", 'Scan Faktur Berhasil!');
                        // document.location.href = '/account-management/permission';
            },
            error: function (response) {
                $.unblockUI();
                // var data2 = data;

                if (response.status == 422){
                    bootbox.confirm(response.responseJSON.message+".<br>Apakah Anda Ingin Menyimpan Ulang ?", function (result) {
                        if(result){
                            // console.log($('#insert_faktur').serialize());
                            $.ajax({
                                type: 'POST',
                                url: '/faktur/scan-replace',
                                data: $('#insert_faktur').serialize(),
                                // data: data2,
                                beforeSend: function () {
                                    $.blockUI({
                                        message: '<i class="icon-spinner4 spinner"></i>',
                                        overlayCSS: {
                                            backgroundColor: '#fff',
                                            opacity: 0.8,
                                            cursor: 'wait'
                                        },
                                        css: {
                                            border: 0,
                                            padding: 0,
                                            backgroundColor: 'transparent'
                                        }
                                    });
                                },
                                complete: function () {
                                    $.unblockUI();
                                },
                                success: function () {
                                    $('#insert_faktur').trigger("reset");
                                    $('#select_bulan').val(bulan);
                                    // req_load_list_issue();
                                    // $("#select_bulan").empty();
                                    // load_bulan();
                                    $('#fakturTable').DataTable().ajax.reload();
                                    $("#alert_success").trigger("click", 'Update Faktur Berhasil!');
                                },
                                error: function (response) {
                                    $.unblockUI();
                                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);

                                }
                            });
                        }
                        else{
                            // $("#alert_warning_2").trigger("click", response.responseJSON.message);
                            $('#insert_faktur').trigger("reset");
                            $('#select_bulan').val(bulan);
                            load_bulan();
                        }
                    });

                }
            }
        });

    });


});


    // function load_bulan() {
    //     $("#select_bulan").empty();
    //     $("#select_bulan").append('<option value="">-- Pilih Bulan --</option>');
    //     $("#select_bulan").append('<option value="1">JANUARI</option>');
    //     $("#select_bulan").append('<option value="2">FEBRUARI</option>');
    //     $("#select_bulan").append('<option value="3">MARET</option>');
    //     $("#select_bulan").append('<option value="4">APRIL</option>');
    //     $("#select_bulan").append('<option value="5">MEI</option>');
    //     $("#select_bulan").append('<option value="6">JUNI</option>');
    //     $("#select_bulan").append('<option value="7">JULI</option>');
    //     $("#select_bulan").append('<option value="8">AGUSTUS</option>');
    //     $("#select_bulan").append('<option value="9">SEPTEMBER</option>');
    //     $("#select_bulan").append('<option value="10">OKTOBER</option>');
    //     $("#select_bulan").append('<option value="11">NOVEMBER</option>');
    //     $("#select_bulan").append('<option value="12">DESEMBER</option>');
    // }


    function fakturTable() {

        $('#fakturTable').DataTable().destroy();
        $('#fakturTable tbody').empty();
        var fakturTable = $('#fakturTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/faktur/data',
                // data: function (d) {
                //     return $.extend({}, d, {
                //         'nik_pegawai': nik_pegawai,
                //     });
                // },
            },
            fnCreatedRow: function (row, data, index) {
                var info = fakturTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                { data: null, sortable: false, orderable: false, searchable: false},
                { data: 'uuid', name: 'id', searchable: true, visible: false, orderable: false },
                { data: 'bulan', name: 'bulan_finance', searchable: true, orderable: true },
                { data: 'nomor_faktur', name: 'nomor_faktur', searchable: true, orderable: true },
                { data: 'tanggal_faktur', name: 'tanggal_faktur', searchable: true, orderable: true },
                // { data: 'npwp_penjual', name: 'npwp_penjual', searchable: true, orderable: true },
                { data: 'nama_penjual', name: 'nama_penjual', searchable: true, orderable: true },
                // { data: 'alamat_penjual', name: 'alamat_penjual', searchable: true, orderable: true },
                // { data: 'npwp_lawan_transaksi', name: 'npwp_lawan_transaksi', searchable: true, orderable: true },
                // { data: 'nama_lawan_transaksi', name: 'nama_lawan_transaksi', searchable: true, orderable: true },
                // { data: 'alamat_lawan_transaksi', name: 'alamat_lawan_transaksi', searchable: true, orderable: true },
                { data: 'jumlah_dpp', name: 'jumlah_dpp', searchable: true, orderable: true },
                { data: 'jumlah_ppn', name: 'jumlah_ppn', searchable: true, orderable: true },
                // { data: 'jumlah_ppnbm', name: 'jumlah_ppnbm', searchable: true, orderable: true },
                // { data: 'referensi', name: 'referensi', searchable: true, orderable: true },
                {data: 'action', name: 'action',searchable:true,orderable:true},
            ]
        });

        var dtable = $('#fakturTable').dataTable().api();
        $(".fakturTable_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        dtable.draw();


    }

    function detail_faktur(url){
		$.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();

			}
		})
		.done(function (response) {
            $('#headerFakturModal').modal();
            $('#npwp_penjual').val(response.faktur.npwp_penjual);
            $('#nama_penjual').val(response.faktur.nama_penjual);
            $('#alamat_penjual').val(response.faktur.alamat_penjual);
            $('#npwp_lawan_transaksi').val(response.faktur.npwp_lawan_transaksi);
            $('#nama_lawan_transaksi').val(response.faktur.nama_lawan_transaksi);
            $('#alamat_lawan_transaksi').val(response.faktur.alamat_lawan_transaksi);
            $('#jumlah_dpp').val(response.faktur.jumlah_dpp);
            $('#jumlah_ppn').val(response.faktur.jumlah_ppn);
            $('#jumlah_ppnbm').val(response.faktur.jumlah_ppnbm);
            $('#referensi').val(response.faktur.referensi);

		});
    }

//     mappings        = JSON.parse($('#mappings').val());
//     var form_status = $('#form_status').val();

//     if(form_status == 'create')
//     {
//         render();
//     }else
//     {
//         var url_data_permission = $('#url_data_permission').val();

//         $('#permissionTable').DataTable().destroy();
//         $('#permissionTable tbody').empty();
//         var table = $('#permissionTable').DataTable({
//             dom: 'Bfrtip',
//             processing: true,
//             serverSide: true,
//             pageLength:10,
//             scrollY:250,
//             scroller:true,
//             destroy:true,
//             deferRender:true,
//             bFilter:true,
//             ajax: {
//                 type: 'GET',
//                 url: url_data_permission,
//             },
//             fnCreatedRow: function (row, data, index) {
//                 var info = table.page.info();
//                 var value = index+1+info.start;
//                 $('td', row).eq(0).html(value);
//             },
//             columns: [
//                 {data: null, sortable: false, orderable: false, searchable: false},
//                 {data: 'display_name', name: 'display_name',searchable:true,orderable:true},
//                 {data: 'action', name: 'action',searchable:false,orderable:false},
//             ]
//         });

//         var dtable2 = $('#permissionTable').dataTable().api();
//         $(".dataTables_filter input")
//             .unbind() // Unbind previous default bindings
//             .bind("keyup", function (e) { // Bind our desired behavior
//                 if (e.keyCode == 13) {
//                     // Call the API search function
//                     dtable2.search(this.value).draw();
//                 }
//                 if (this.value == "") {
//                     dtable2.search("").draw();
//                 }
//                 return;
//         });
//         dtable2.draw();

//         render();
//     }

// $('#select_permission').on('change',function()
// {
//     var value = $(this).val();
//     var name = $(this).select2('data')[0].text;
//     var form_status = $('#form_status').val();

//     if(value)
//     {
//         var input = {
//             'id': value,
//             'name': name
//         };

//         var diff = checkItem(value);
//         if (!diff)
//         {
//             $("#alert_warning").trigger("click", name+' already selected.');
//             $(this).val('').trigger('change');
//             return false;
//         }

//         if(form_status == 'edit')
//         {
//             var role_id = $('#role_id').val();
//             $.ajaxSetup({
//                 headers: {
//                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                 }
//             });
//             $.ajax({
//                 type: 'post',
//                 url: '/account-management/role/store/permission',
//                 data: {
//                     role_id: role_id,
//                     permission_id: value
//                 }
//             })
//             .done(function () {
//                 $('#permissionTable').DataTable().ajax.reload();
//             });
//         }

//         if (name) mappings.push(input);

//         render();
//         $(this).val('').trigger('change');
//     }
// });

// function hapus(url)
// {
//     $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });

//     $.ajax({
//         type: "delete",
//         url: url,
//         beforeSend: function () {
//             $.blockUI({
//                 message: '<i class="icon-spinner4 spinner"></i>',
//                 overlayCSS: {
//                     backgroundColor: '#fff',
//                     opacity: 0.8,
//                     cursor: 'wait'
//                 },
//                 css: {
//                     border: 0,
//                     padding: 0,
//                     backgroundColor: 'transparent'
//                 }
//             });
//         },
//         success: function () {
//             $.unblockUI();
//         },
//         error: function () {
//             $.unblockUI();
//         }
//     })
//     .done(function () {
//         $('#roleTable').DataTable().ajax.reload();
//         $("#alert_success").trigger("click", 'Data Berhasil hapus');
//     });
// }

// function hapusModal(url)
// {
//     $.ajaxSetup({
//         headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//         }
//     });

//     $.ajax({
//         type: "post",
//         url: url,
//         beforeSend: function () {
//             mappings = [];
//         },
//         success: function (response) {
//             var permissions = response;
//             for (idx in permissions)
//             {
//                 var data = permissions[idx];
//                 var input = {
//                     'id': data.id,
//                     'name': data.display_name
//                 };
//                 mappings.push(input);
//             }


//         }
//     })
//     .done(function () {
//         $('#permissionTable').DataTable().ajax.reload();
//         render();
//     });
// }

// function render()
// {
//     getIndex();
//     $('#mappings').val(JSON.stringify(mappings));

//     var tmpl = $('#permission_table').html();
//     Mustache.parse(tmpl);
//     var data = { item: mappings };
//     var html = Mustache.render(tmpl, data);
//     $('#role_permission').html(html);
//     bind();
// }

// function bind()
// {
//     $('.btn-delete-item').on('click', deleteItem);
// }

// function getIndex()
// {
//     for (idx in mappings)
//     {
//         mappings[idx]['_id'] = idx;
//         mappings[idx]['no'] = parseInt(idx) + 1;
//     }
// }

// function deleteItem()
// {
//     var i = parseInt($(this).data('id'), 10);

//     mappings.splice(i, 1);
//     render();
// }

// function checkItem(id)
// {
//     for (var i in mappings)
//     {
//         var data = mappings[i];

//         if (data.id == id)
//             return false;
//     }

//     return true;
// }

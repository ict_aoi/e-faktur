<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    @if(auth::user()->photo)
                        <img src="{{ route('accountSetting.showAvatar', auth::user()->photo) }}" class="img-circle" alt="profile_photo" id="avatar_image">
                    @else
                        {{-- @if(strtoupper(auth::user()->sex) == 'LAKI') --}}
                            <a href="#"><img src="{{ asset('images/male_avatar.png') }}" class="img-circle" alt="avatar_male"></a>
                        {{-- @else
                            <a href="#"><img src="{{ asset('images/female_avatar.png') }}" class="img-circle" alt="avatar_female"></a>
                        @endif --}}
                    @endif
                    <h6>{{ Auth::user()->name }}</h6>
                    <span class="text-size-small">{{ Auth::user()->email }}</span>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>Akun saya</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li class="divider"></li>
                    <li class="{{ $active == 'account_setting' ? 'active' : '' }}"><a href="{{ route('accountSetting') }}"><i class="icon-cog5"></i> <span>Pengaturan Akun</span></a></li>
                    <li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Keluar</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{ $active == 'dashboard' ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>

                    @permission(['menu-user','menu-role','menu-permission'])
                    <li class="{{ (in_array($active,['permission','role','user'])) ? 'active' : '' }}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-users"></i> <span>Account Management</span></a>
                        <ul>
                            @permission('menu-permission')
                                <li class="{{ $active == 'permission' ? 'active' : '' }}"><a href="{{ route('permission.index') }}" class="legitRipple">Permission</a></li>
                            @endpermission
                            @permission('menu-role')
                                <li class="{{ $active == 'role' ? 'active' : '' }}"><a href="{{ route('role.index') }}" class="legitRipple">Role</a></li>
                            @endpermission
                            @permission('menu-user')
                                <li class="{{ $active == 'user' ? 'active' : '' }}"><a href="{{ route('user.index') }}" class="legitRipple">User</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission

                    @permission(['menu-scan','menu-report','menu-lock'])
                    <li class="{{ (in_array($active,['faktur-scan','faktur-report','faktur-lock'])) ? 'active' : '' }}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-certificate"></i> <span>Faktur</span></a>
                        <ul>
                            @permission('menu-scan')
                                <li class="{{ $active == 'faktur-scan' ? 'active' : '' }}"><a href="{{ route('faktur.index') }}" class="legitRipple">Scan Faktur</a></li>
                            @endpermission
                            @permission('menu-report')
                                <li class="{{ $active == 'faktur-report' ? 'active' : '' }}"><a href="{{ route('faktur.report') }}" class="legitRipple">Report Faktur</a></li>
                            @endpermission
                            {{-- @permission('menu-lock')
                                <li class="{{ $active == 'faktur-lock' ? 'active' : '' }}"><a href="{{ route('faktur.lock') }}" class="legitRipple">Close Faktur</a></li>
                            @endpermission --}}
                        </ul>
                    </li>
                    @endpermission
                    <!-- END OF MASTER DATA -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>

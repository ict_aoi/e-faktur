@extends('layouts.app', ['active' => 'faktur-report'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Faktur</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Faktur</li>
				<li class="active">Report</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			{{-- @include('form.select', [
				'field'     => 'bulan',
                'label'     => 'Bulan',
                'mandatory' => '*Required',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'options'   => [
                    ''   => '-- Select Bulan --',
                    '1'  => 'JANUARI',
                    '2'  => 'FEBRUARI',
                    '3'  => 'MARET',
                    '4'  => 'APRIL',
                    '5'  => 'MEI',
                    '6'  => 'JUNI',
                    '7'  => 'JULI',
                    '8'  => 'AGUSTUS',
                    '9'  => 'SEPTEMBER',
                    '10' => 'OKTOBER',
                    '11' => 'NOVEMBER',
                    '12' => 'DESEMBER',
                ],
                'class'      => 'select-search',
                'attributes' => [
                    'id' => 'select_bulan'
                ]
            ])

            @include('form.select', [
				'field'     => 'tahun',
                'label'     => 'Tahun',
                'mandatory' => '*Required',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'options'   => [
                    ''   => '-- Select Tahun --',
                ]+$list_array,
                'class'      => 'select-search',
                'attributes' => [
                    'id' => 'select_tahun'
                ]
            ]) --}}
            
            @include('form.date', [
				'field' 		=> 'start_date',
				'label' 		=> 'Scan Date From',
				'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
				'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
			])
				
			@include('form.date', [
				'field' 		=> 'end_date',
				'label' 		=> 'Scan Date To',
				'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
				'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])

			@include('form.select', [
				'field'     => 'factory',
				'label'     => 'Factory',
				'mandatory' => '*Required',
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
				'options'   => [
					''                => '-- Select Factory --',
					'031233745503000' => 'AOI 1',
					'031233745503001' => 'AOI 2',
					'013459292503001' => 'BBI S',
				],
				'class'      => 'select-search',
				'attributes' => [
					'id' => 'select_factory'
				]
			])

			{!!
				Form::open(array(
					'class'  => 'heading-form',
					'role'   => 'form',
					'url'    => route('faktur.exportReport'),
					'method' => 'get',
					'target' => '_blank',
					'id'     => 'export_report'
				))
			!!}
			{!! Form::hidden('_factory', '',array('id' => '_factory')) !!}
			{!! Form::hidden('_start_date', '',array('id' => '_start_date')) !!}
			{!! Form::hidden('_end_date', '',array('id' => '_end_date')) !!}
			<button type="submit" class="btn btn-default col-xs-12">Export Report Faktur <i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="report_faktur_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>No</th>
							<th>FM</th>
							<th>Kode Transkasi</th>
							<th>FG Pengganti</th>
							<th>Nomor Faktur</th>
							<th>Masa Pajak</th>
							<th>Tahun Pajak</th>
							<th>Tanggal Faktur</th>
							<th>NPWP Penjual</th>
							<th>Nama Penjual</th>
							{{-- <th>Alamat Pembeli</th> --}}
							<th>Jumlah DPP</th>
							<th>Jumlah PPN</th>
							<th>Jumlah PPNBM</th>
							<th>Creditable</th>
							<th>Referensi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{{-- {!! Form::hidden('page','index', array('id' => 'page')) !!} --}}
@endsection

@section('page-js')
	<script src="{{ mix('js/faktur_report.js') }}"></script>
@endsection

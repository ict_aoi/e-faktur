@extends('layouts.app', ['active' => 'faktur-lock'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Faktur</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Faktur</li>
				<li class="active">Lock</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">

            {!!
                Form::open([
                    'role'   => 'form',
                    'url'    => route('faktur.lockFaktur'),
                    'method' => 'post',
                    'class'  => 'form-horizontal',
                    'id'     => 'lock_faktur'
                ])
            !!}

                @include('form.select', [
                    'field'     => 'bulan',
                    'label'     => 'Bulan',
                    'mandatory' => '*Required',
                    'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                    'options'   => [
                        ''   => '-- Select Bulan --',
                        '1'  => 'JANUARI',
                        '2'  => 'FEBRUARI',
                        '3'  => 'MARET',
                        '4'  => 'APRIL',
                        '5'  => 'MEI',
                        '6'  => 'JUNI',
                        '7'  => 'JULI',
                        '8'  => 'AGUSTUS',
                        '9'  => 'SEPTEMBER',
                        '10' => 'OKTOBER',
                        '11' => 'NOVEMBER',
                        '12' => 'DESEMBER',
                    ],
                    'class'      => 'select-search',
                    'attributes' => [
                        'id' => 'select_bulan'
                    ]
                ])
            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            {!! Form::close() !!}
		</div>
	</div>

	{{-- {!! Form::hidden('page','index', array('id' => 'page')) !!} --}}
@endsection

@section('page-js')
	<script src="{{ mix('js/faktur_lock.js') }}"></script>
@endsection

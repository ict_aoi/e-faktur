<div id="headerFakturModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				<div class="modal-body">
					@include('form.text', [
						'field'      => 'npwp_penjual',
						'label'      => 'NPWP Penjual',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'npwp_penjual',
							'readonly' => '',
						]
					])
					@include('form.text', [
						'field'      => 'nama_penjual',
						'label'      => 'Nama Penjual',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'nama_penjual',
							'readonly' => '',
						]
					])
					@include('form.textarea', [
						'field'      => 'alamat_penjual',
						'label'      => 'Alamat Penjual',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'alamat_penjual',
							'rows'     => 3,
							'style'    => 'resize: none;',
							'readonly' => '',
						]
					])
					@include('form.text', [
						'field'      => 'npwp_lawan_transaksi',
						'label'      => 'NPWP Pembeli',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'npwp_lawan_transaksi',
							'readonly' => '',
						]
					])
					@include('form.text', [
						'field'      => 'nama_lawan_transaksi',
						'label'      => 'Nama Pembeli',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'nama_lawan_transaksi',
							'readonly' => '',
						]
					])
					@include('form.textarea', [
						'field'      => 'alamat_lawan_transaksi',
						'label'      => 'Alamat Pembeli',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'alamat_lawan_transaksi',
							'rows'     => 3,
							'style'    => 'resize: none;',
							'readonly' => '',
						]
					])
					@include('form.text', [
						'field'      => 'jumlah_dpp',
						'label'      => 'Jumlah DPP',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'jumlah_dpp',
							'readonly' => '',
						]
					])
					@include('form.text', [
						'field'      => 'jumlah_ppn',
						'label'      => 'Jumlah PPN',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'jumlah_ppn',
							'readonly' => '',
						]
					])
					@include('form.text', [
						'field'      => 'jumlah_ppnbm',
						'label'      => 'Jumlah PPNBM',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'jumlah_ppnbm',
							'readonly' => '',
						]
					])
					@include('form.text', [
						'field'      => 'referensi',
						'label'      => 'Referensi',
						'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id'       => 'referensi',
							'readonly' => '',
						]
					])
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					{{-- <button type="submit" class="btn btn-primary">Submit form</button> --}}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
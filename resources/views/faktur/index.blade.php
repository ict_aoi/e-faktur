@extends('layouts.app',['active' => 'faktur-scan'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Faktur</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li >Faktur</li>
            <li class="active">Scan</li>
        </ul>
    </div>
</div>
@endsection

@section('page-modal')
	@include('faktur._detail_modal')
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
        {!!
            Form::open([
                'role'   => 'form',
                'url'    => route('faktur.scan'),
                'method' => 'post',
                'class'  => 'form-horizontal',
                'id'     => 'insert_faktur'
            ])
        !!}

            @include('form.select', [
                'field'     => 'bulan',
                'label'     => 'Bulan',
                'mandatory' => '*Required',
                'default'   => $now,
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'options'   => [
                    ''   => '-- Select Bulan --',
                    '1'  => 'JANUARI',
                    '2'  => 'FEBRUARI',
                    '3'  => 'MARET',
                    '4'  => 'APRIL',
                    '5'  => 'MEI',
                    '6'  => 'JUNI',
                    '7'  => 'JULI',
                    '8'  => 'AGUSTUS',
                    '9'  => 'SEPTEMBER',
                    '10' => 'OKTOBER',
                    '11' => 'NOVEMBER',
                    '12' => 'DESEMBER',
                ],
                'class'      => 'select-search',
                'attributes' => [
                    'id' => 'select_bulan'
                ]
            ])

            @include('form.text', [
                'field'      => 'url_pajak',
                'label'      => 'Scan Disini!',
                'mandatory'  => '*Required',
                'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'url_pajak'
                ]
            ])

            <div class="text-right">
            <button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        {!! Form::close() !!}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-basic table-striped table-hover" id="fakturTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Id</th>
                        <th>Bulan</th>
                        <th>Nomor Faktur</th>
                        <th>Tanggal Faktur</th>
                        {{-- <th>NPWP Penjual</th> --}}
                        <th>Nama Penjual</th>
                        {{-- <th>Alamat Penjual</th>
                        <th>NPWP Pembeli</th>
                        <th>Nama Pembeli</th>
                        <th>Alamat Pembeli</th> --}}
                        <th>Jumlah DPP</th>
                        <th>Jumlah PPN</th>
                        <th>Action</th>
                        {{-- <th>Jumlah PPNBM</th> --}}
                        {{-- <th>Referensi</th> --}}
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{{-- {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!} --}}
@endsection

@section('page-js')
<script src="{{ mix('js/faktur.js') }}"></script>
@endsection

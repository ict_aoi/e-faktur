<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailFaktursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_fakturs', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('faktur_id',36);
            $table->string('nama')->nullable();
            $table->double('harga_satuan',25,8)->nullable();
            $table->double('jumlah_barang',25,8)->nullable();
            $table->double('harga_total',25,8)->nullable();
            $table->double('diskon',25,8)->nullable();
            $table->double('dpp',25,8)->nullable();
            $table->double('ppn',25,8)->nullable();
            $table->double('tarif_ppnbm',25,8)->nullable();
            $table->double('ppnbm',25,8)->nullable();            
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('faktur_id')->references('id')->on('fakturs')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_fakturs');
    }
}

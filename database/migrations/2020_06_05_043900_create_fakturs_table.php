<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaktursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fakturs', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('kode_jenis')->nullable();
            $table->string('fg_pengganti')->nullable();
            $table->string('nomor_faktur')->nullable();
            $table->string('bulan_finance')->nullable();
            $table->string('factory')->nullable();
            $table->date('tanggal_faktur')->nullable();
            $table->string('npwp_penjual')->nullable();
            $table->string('nama_penjual')->nullable();
            $table->string('alamat_penjual')->nullable();
            $table->string('npwp_lawan_transaksi')->nullable();
            $table->string('nama_lawan_transaksi')->nullable();
            $table->string('alamat_lawan_transaksi')->nullable();
            $table->double('jumlah_dpp',25,8)->nullable();
            $table->double('jumlah_ppn',25,8)->nullable();
            $table->double('jumlah_ppnbm',25,8)->nullable();
            $table->string('status_approval')->nullable();
            $table->string('status_faktur')->nullable();
            $table->string('referensi')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fakturs');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account-setting', 'HomeController@accountSetting')->name('accountSetting');
Route::put('/account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
Route::get('/account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');
Route::post('account-setting/{id}/update', 'HomeController@updateAccount')->name('accountSetting.updateAccount');


Route::prefix('account-management')->group(function () {
    Route::prefix('permission')->middleware(['permission:menu-permission'])->group(function () {
        Route::get('', 'PermissionController@index')->name('permission.index');
        Route::get('create', 'PermissionController@create')->name('permission.create');
        Route::get('data', 'PermissionController@data')->name('permission.data');
        Route::post('store', 'PermissionController@store')->name('permission.store');
        Route::get('edit/{id}', 'PermissionController@edit')->name('permission.edit');
        Route::post('update/{id}', 'PermissionController@update')->name('permission.update');
        Route::delete('delete/{id}', 'PermissionController@destroy')->name('permission.destroy');
    });

    Route::prefix('role')->middleware(['permission:menu-role'])->group(function () {
        Route::get('', 'RoleController@index')->name('role.index');
        Route::get('create', 'RoleController@create')->name('role.create');
        Route::get('data', 'RoleController@data')->name('role.data');
        Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
        Route::get('edit/{id}/permission-role', 'RoleController@dataPermission')->name('role.dataPermission');
        Route::post('store', 'RoleController@store')->name('role.store');
        Route::post('store/permission', 'RoleController@storePermission')->name('role.storePermission');
        Route::post('delete/{role_id}/{permission_id}/permission-role', 'RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
        Route::post('update/{id}', 'RoleController@update')->name('role.update');
        Route::delete('delete/{id}', 'RoleController@destroy')->name('role.destroy');
    });

    Route::prefix('user')->middleware(['permission:menu-user'])->group(function () {
        Route::get('', 'UserController@index')->name('user.index');
        Route::get('create', 'UserController@create')->name('user.create');
        Route::get('data', 'UserController@data')->name('user.data');
        Route::get('get-absence', 'UserController@getAbsence')->name('user.getAbsence');
        Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
        Route::get('edit/{id}/role-user', 'UserController@dataRole')->name('user.dataRole');
        Route::post('store', 'UserController@store')->name('user.store');
        Route::post('store/role', 'UserController@storeRole')->name('user.storeRole');
        Route::post('delete/{user_id}/{role_id}/role-user', 'UserController@destroyRoleUser')->name('user.destroyRoleUser');
        Route::post('update/{id}', 'UserController@update')->name('user.update');
        Route::put('reset-password/{id}', 'UserController@resetPassword')->name('user.resetPassword');
        Route::put('delete/{id}', 'UserController@destroy')->name('user.destroy');
    });
});

Route::prefix('faktur')->group(function () {
    Route::get('', 'FakturController@index')->name('faktur.index');
    Route::post('scan', 'FakturController@scan')->name('faktur.scan');
    Route::post('scan-replace', 'FakturController@scanReplace')->name('faktur.scanReplace');
    Route::get('data', 'FakturController@data')->name('faktur.data');
    Route::get('detail/{id}', 'FakturController@detail')->name('faktur.detail');
    Route::get('report', 'FakturController@report')->name('faktur.report');
    Route::get('data-report', 'FakturController@dataReport')->name('faktur.dataReport');
    Route::get('export-report', 'FakturController@exportReport')->name('faktur.exportReport');
    Route::get('close', 'FakturController@lock')->name('faktur.lock');
    Route::post('close-faktur', 'FakturController@lockFaktur')->name('faktur.lockFaktur');
});

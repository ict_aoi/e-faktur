<?php

namespace App\Models;

use App\Uuids;
use DB;
use Illuminate\Database\Eloquent\Model;

class DetailFaktur extends Model
{
    use Uuids;
    public    $incrementing = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['faktur_id', 'nama', 'harga_satuan', 'jumlah_barang', 'harga_total', 'diskon', 'dpp',
    'ppn', 'tarif_ppnbm', 'ppnbm','deleted_at'];
    protected $dates        = ['created_at','deleted_at'];

    public function Faktur()
    {
        return $this->belongsTo('App\Models\Faktur');
    }
}

<?php

namespace App\Models;

use App\Uuids;
use DB;
use Illuminate\Database\Eloquent\Model;

class Faktur extends Model
{
    use Uuids;
    public    $incrementing = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['kode_jenis', 'fg_pengganti', 'nomor_faktur', 'bulan_finance', 'factory',
    'tanggal_faktur', 'npwp_penjual', 'nama_penjual', 'alamat_penjual', 'npwp_lawan_transaksi', 'nama_lawan_transaksi',
    'alamat_lawan_transaksi', 'jumlah_dpp', 'jumlah_ppn', 'jumlah_ppnbm', 'status_approval', 'status_faktur',
    'referensi', 'deleted_at', 'created_by'];
    protected $dates        = ['created_at','deleted_at'];
}

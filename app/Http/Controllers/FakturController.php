<?php

namespace App\Http\Controllers;

use DB;
use File;
use Auth;
use Mail;
use Excel;
use StdClass;
use Validator;
use PHPExcel_Cell_DataType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Routing\UrlGenerator;


use App\Models\Faktur;
use App\Models\DetailFaktur;

class FakturController extends Controller
{
    public function index()
    {
        // $issue = IssueCategory::pluck('name', 'id')->all();
        $carbon = Carbon::now();
        $now    = $carbon->month;

        return view('faktur.index',compact('now'));
    }

    public function scan(Request $request)
    {
        //name
        $this->validate($request, [
            // 'factory'   => 'required',
            'bulan'     => 'required',
            'url_pajak' => 'required'
        ]);

        $carbon = Carbon::now();

        // dd($request->all());
        // if(Permission::where('name',str_slug($request->name))->exists())
        //     return response()->json(['message' => 'Nama sudah ada, silahkan cari nama permission lain.'], 422);


        $xml = (array) simplexml_load_string(file_get_contents($request->url_pajak));

        if ($xml['statusApproval'] != 'Faktur tidak Valid, Tidak ditemukan data di DJP') {

            $cek_faktur = Faktur::whereNull('deleted_at')->where([['nomor_faktur',$xml['nomorFaktur']]])->exists();

            if($cek_faktur == false){

                if($xml['npwpLawanTransaksi'] == '031233745503000'){
                    $npwp = '1000001';
                }
                else if($xml['npwpLawanTransaksi'] == '031233745503001'){
                    $npwp = '1000002';
                }
                else if($xml['npwpLawanTransaksi'] == '013459292503001'){
                    $npwp = 'BBIS';
                }
                else{
                    $npwp = '-';
                }

                if(array_key_exists("referensi",$xml)){
                    $referensi = $xml['referensi'];
                }
                else{
                    $referensi = null;
                }

                try
                {
                    DB::beginTransaction();

                    $faktur = Faktur::firstorCreate([
                        'kode_jenis'             => $xml['kdJenisTransaksi'],
                        'fg_pengganti'           => $xml['fgPengganti'],
                        'nomor_faktur'           => $xml['nomorFaktur'],
                        'bulan_finance'          => $request->bulan,
                        'factory'                => $npwp,
                        'tanggal_faktur'         => Carbon::createFromFormat('d/m/Y',$xml['tanggalFaktur'])->format('Y-m-d'),
                        'npwp_penjual'           => $xml['npwpPenjual'],
                        'nama_penjual'           => $xml['namaPenjual'],
                        'alamat_penjual'         => $xml['alamatPenjual'],
                        'npwp_lawan_transaksi'   => $xml['npwpLawanTransaksi'],
                        'nama_lawan_transaksi'   => $xml['namaLawanTransaksi'],
                        'alamat_lawan_transaksi' => $xml['alamatLawanTransaksi'],
                        'jumlah_dpp'             => $xml['jumlahDpp'],
                        'jumlah_ppn'             => $xml['jumlahPpn'],
                        'jumlah_ppnbm'           => $xml['jumlahPpnBm'],
                        'status_approval'        => $xml['statusApproval'],
                        'status_faktur'          => $xml['statusFaktur'],
                        'referensi'              => $referensi,
                        'created_by'             => Auth::user()->nik
                    ]);

                    $fakturBarang = $xml['detailTransaksi'];

                    if(is_array($fakturBarang)){
                        foreach ($fakturBarang as $key => $value) {
                            DetailFaktur::firstorCreate([
                                'faktur_id'     => $faktur->id,
                                'nama'          => $value->nama,
                                'harga_satuan'  => $value->hargaSatuan,
                                'jumlah_barang' => $value->jumlahBarang,
                                'harga_total'   => $value->hargaTotal,
                                'diskon'        => $value->diskon,
                                'dpp'           => $value->dpp,
                                'ppn'           => $value->ppn,
                                'tarif_ppnbm'   => $value->tarifPpnbm,
                                'ppnbm'         => $value->ppnbm
                            ]);
                        }
                    }
                    else{
                        DetailFaktur::firstorCreate([
                            'faktur_id'     => $faktur->id,
                            'nama'          => $fakturBarang->nama,
                            'harga_satuan'  => $fakturBarang->hargaSatuan,
                            'jumlah_barang' => $fakturBarang->jumlahBarang,
                            'harga_total'   => $fakturBarang->hargaTotal,
                            'diskon'        => $fakturBarang->diskon,
                            'dpp'           => $fakturBarang->dpp,
                            'ppn'           => $fakturBarang->ppn,
                            'tarif_ppnbm'   => $fakturBarang->tarifPpnbm,
                            'ppnbm'         => $fakturBarang->ppnbm
                        ]);
                    }

                    DB::commit();
                    $request->session()->flash('message', 'success');
                    return response()->json(['faktur_id' => $faktur->id ,'success',200]);

                } catch (Exception $e)
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                $request->session()->flash('message', 'success');
                return response()->json(['success',200]);
            }
            else{
                return response()->json(['message' => 'Faktur Dengan No. '.$xml['nomorFaktur'].' Sudah Pernah Di Scan'], 422);
            }

        } else {
            return response()->json(['message' => 'Faktur Tidak Valid, Tidak Ditemukan Data Di DJP.'], 422);
        }
    }

    public function scanReplace(Request $request)
    {
        //name
        // dd($request);
        $this->validate($request, [
            // 'factory'   => 'required',
            'bulan'     => 'required',
            'url_pajak' => 'required'
        ]);

        $carbon = Carbon::now();

        $xml = (array) simplexml_load_string(file_get_contents($request->url_pajak));

        if ($xml['statusApproval'] != 'Faktur tidak Valid, Tidak ditemukan data di DJP') {

            $cek_faktur = Faktur::whereNull('deleted_at')->where([['nomor_faktur',$xml['nomorFaktur']]])->first();

            if($cek_faktur!=null){
                // dd($cek_faktur);
                $cek_faktur->deleted_at = $carbon;
                // $cek_faktur->deleted_by = Auth::user()->nik;
                $cek_faktur->save();
            }

            if($xml['npwpLawanTransaksi'] == '031233745503000'){
                $npwp = '1000001';
            }
            else if($xml['npwpLawanTransaksi'] == '031233745503001'){
                $npwp = '1000002';
            }
            else if($xml['npwpLawanTransaksi'] == '013459292503001'){
                $npwp = 'BBIS';
            }
            else{
                $npwp = '-';
            }

            if(array_key_exists("referensi",$xml)){
                $referensi = $xml['referensi'];
            }
            else{
                $referensi = null;
            }

            try
            {
                DB::beginTransaction();

                $faktur = Faktur::create([
                    'kode_jenis'             => $xml['kdJenisTransaksi'],
                    'fg_pengganti'           => $xml['fgPengganti'],
                    'nomor_faktur'           => $xml['nomorFaktur'],
                    'bulan_finance'          => $request->bulan,
                    'factory'                => $npwp,
                    'tanggal_faktur'         => Carbon::createFromFormat('d/m/Y',$xml['tanggalFaktur'])->format('Y-m-d'),
                    'npwp_penjual'           => $xml['npwpPenjual'],
                    'nama_penjual'           => $xml['namaPenjual'],
                    'alamat_penjual'         => $xml['alamatPenjual'],
                    'npwp_lawan_transaksi'   => $xml['npwpLawanTransaksi'],
                    'nama_lawan_transaksi'   => $xml['namaLawanTransaksi'],
                    'alamat_lawan_transaksi' => $xml['alamatLawanTransaksi'],
                    'jumlah_dpp'             => $xml['jumlahDpp'],
                    'jumlah_ppn'             => $xml['jumlahPpn'],
                    'jumlah_ppnbm'           => $xml['jumlahPpnBm'],
                    'status_approval'        => $xml['statusApproval'],
                    'status_faktur'          => $xml['statusFaktur'],
                    'referensi'              => $referensi,
                    'created_by'             => Auth::user()->nik
                ]);

                $fakturBarang = $xml['detailTransaksi'];

                if(is_array($fakturBarang)){
                    foreach ($fakturBarang as $key => $value) {
                        DetailFaktur::create([
                            'faktur_id'     => $faktur->id,
                            'nama'          => $value->nama,
                            'harga_satuan'  => $value->hargaSatuan,
                            'jumlah_barang' => $value->jumlahBarang,
                            'harga_total'   => $value->hargaTotal,
                            'diskon'        => $value->diskon,
                            'dpp'           => $value->dpp,
                            'ppn'           => $value->ppn,
                            'tarif_ppnbm'   => $value->tarifPpnbm,
                            'ppnbm'         => $value->ppnbm
                        ]);
                    }
                }
                else{
                    DetailFaktur::create([
                        'faktur_id'     => $faktur->id,
                        'nama'          => $fakturBarang->nama,
                        'harga_satuan'  => $fakturBarang->hargaSatuan,
                        'jumlah_barang' => $fakturBarang->jumlahBarang,
                        'harga_total'   => $fakturBarang->hargaTotal,
                        'diskon'        => $fakturBarang->diskon,
                        'dpp'           => $fakturBarang->dpp,
                        'ppn'           => $fakturBarang->ppn,
                        'tarif_ppnbm'   => $fakturBarang->tarifPpnbm,
                        'ppnbm'         => $fakturBarang->ppnbm
                    ]);
                }

                DB::commit();
                $request->session()->flash('message', 'success');
                // return response()->json(['faktur_id' => $faktur->id ,'success',200]);

            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            $request->session()->flash('message', 'success');
            return response()->json(['success',200]);

        } else {
            return response()->json(['message' => 'Faktur Tidak Valid, Tidak Ditemukan Data Di DJP.'], 422);
        }
    }

    public function data()
    {
        if(request()->ajax())
        {
            $nik  = Auth::user()->nik;
            $data = Faktur::whereNull('deleted_at')
            ->orderBy('created_at','desc')
            ->where('created_by',$nik)->take(10);

            return datatables()->of($data)
            ->editColumn('uuid',function($data){
                return $data->id;
            })
            ->editColumn('bulan',function($data){
                return strtoupper($data->bulan_finance);
            })
            ->addColumn('action', function($data) {
                return view('faktur._action', [
                    'model'  => $data,
                    'detail' => route('faktur.detail',$data->id),
                ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function detail($id)
    {
        $faktur = Faktur::find($id);
        // if($request->session()->has('message')) $request->session()->forget('message');
        // return view('faktur.detail',compact('faktur'));

        $obj         = new StdClass();
        $obj->faktur = $faktur;

        // dd($obj);
        // dd($faktur->get());

		return response()->json($obj,200);
    }

    public function report()
    {
        // $carbon = Carbon::now();
        // $now    = $carbon->year;
        // $last   = $carbon->subYears(5)->year;

        // $years = range($now, $last);

        // $list_array = array(
        //     $now   => $now,
        //     $now-1 => $now-1,
        //     $now-2 => $now-2,
        //     $now-3 => $now-3,
        //     $now-4 => $now-4,
        //     $now-5 => $now-5,
        // );


        // return view('faktur.report',compact('list_array'));
        return view('faktur.report');
    }

    public function dataReport(Request $request)
    {
        if(request()->ajax())
        {
            $req_start_date = $request->start_date;
            $req_end_date   = $request->end_date;
            $npwp           = $request->factory;

            if($req_start_date == null || $req_end_date == null){
                $start_date = null;
                $end_date   = null;
            }
            else{
                $start_date = Carbon::createFromFormat('d/m/Y',$req_start_date)->format('Y-m-d');
                $end_date   = Carbon::createFromFormat('d/m/Y',$req_end_date)->format('Y-m-d');
            }

            if($start_date == $end_date){
                $data = Faktur::whereNull('deleted_at')
                ->where([['npwp_lawan_transaksi',$npwp],[db::raw('date(created_at)'),$start_date]])
                // ->whereBetween('created_at',[$start_date,$end_date])
                ->orderby('created_at','desc');
            }
            else{
                $data = Faktur::whereNull('deleted_at')
                ->where('npwp_lawan_transaksi',$npwp)
                ->whereBetween('created_at',[$start_date,$end_date])
                ->orderby('created_at','desc');
            }

            return datatables()->of($data)
            ->editColumn('uuid',function($data){
                return $data->id;
            })
            ->addColumn('fm', function($data) {
                return  'FM';
            })
            ->addColumn('tahun_pajak', function($data) {
                $tahun_pajak = Carbon::parse($data->created_at)->format('Y');

                return  $tahun_pajak;
            })
            ->addColumn('creditable', function($data) {
                if($data->kode_jenis == '01' || $data->kode_jenis == '04'){
                    return '1';
                }
                else if($data->kode_jenis == '07' || $data->kode_jenis == '08'){
                    return '0';
                }

                else{
                    return '-';
                }
            })
            ->rawColumns(['tahun_pajak','creditable'])
            ->make(true);
        }
    }


    public function exportReport(Request $request)
    {
        // if(request()->ajax())
        // {)
            $req_start_date = $request->_start_date;
            $req_end_date   = $request->_end_date;
            $npwp           = $request->_factory;

            if($req_start_date == null || $req_end_date == null){
                $start_date = null;
                $end_date   = null;
            }
            else{
                $start_date = Carbon::createFromFormat('d/m/Y',$req_start_date)->format('Y-m-d');
                $end_date   = Carbon::createFromFormat('d/m/Y',$req_end_date)->format('Y-m-d');
            }

            if($start_date == $end_date){
                $efakturs = Faktur::whereNull('deleted_at')
                ->where([['npwp_lawan_transaksi',$npwp],[db::raw('date(created_at)'),$start_date]])
                // ->whereBetween('created_at',[$start_date,$end_date])
                ->orderby('created_at','asc')
                ->get();
            }
            else{
                $efakturs = Faktur::whereNull('deleted_at')
                ->where('npwp_lawan_transaksi',$npwp)
                ->whereBetween('created_at',[$start_date,$end_date])
                ->orderby('created_at','asc')
                ->get();
            }

            // foreach($efakturs as $e){
            //     echo $e->factory;
            //     echo '<br>';
            // }
            // die();
            return Excel::create('REPORT EFAKTUR',function ($excel) use ($efakturs){

                $excel->sheet('active', function($sheet) use ($efakturs) {
                    $sheet->setCellValue('A1','NO');
                    $sheet->setCellValue('B1','FACTORY');
                    $sheet->setCellValue('C1','FM');
                    $sheet->setCellValue('D1','KODE JENIS TRANSAKSI');
                    $sheet->setCellValue('E1','FG PENGGANTI');
                    $sheet->setCellValue('F1','NOMOR FAKTUR');
                    $sheet->setCellValue('G1','MASA PAJAK');
                    $sheet->setCellValue('H1','TAHUN PAJAK');
                    $sheet->setCellValue('I1','TANGGAL FAKTUR');
                    $sheet->setCellValue('J1','NPWP PENJUAL',PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue('K1','NAMA PENJUAL');
                    $sheet->setCellValue('L1','ALAMAT PENJUAL');
                    $sheet->setCellValue('M1','JUMLAH DPP');
                    $sheet->setCellValue('N1','JUMLAH PPN');
                    $sheet->setCellValue('O1','JUMLAH PPNBM');
                    $sheet->setCellValue('P1','CREDITABLE');
                    $sheet->setCellValue('Q1','REFERENSI');
                    // $sheet->setWidth('A', 15);
                    $sheet->setColumnFormat(array(
                        'A' => '@',
                        'B' => '@',
                        'C' => '@',
                        'D' => '@',
                        'E' => '@',
                        'F' => '@',
                        'G' => '@',
                        'H' => '@',
                        'I' => '@',
                        'J' => '0',
                        'K' => '@',
                        'L' => '@',
                        'M' => '0',
                        'N' => '0',
                        'O' => '@',
                        'P' => '@',
                        'Q' => '@',
                    ));

                    $index = 0;
                    foreach ($efakturs as $key => $efaktur) {
                        if($efaktur->kode_jenis == '01' || $efaktur->kode_jenis == '04'){
                            $creditable = '1';
                        }
                        else if($efaktur->kode_jenis == '07' || $efaktur->kode_jenis == '08'){
                            $creditable = '0';
                        }
                        else{
                            $creditable = '-';
                        }

                        if($efaktur->factory == '1000001'){
                            $factory_id = 'AOI 1';
                        }else if($efaktur->factory == '1000002'){
                            $factory_id = 'AOI 2';
                        }
                        else{
                            $factory_id = 'BBIS';
                        }

                        $tanggal_scan  = Carbon::parse($efaktur->created_at);
                        $tanggal_pajak = Carbon::createFromFormat('Y-m-d',$efaktur->tanggal_faktur)->format('d/m/Y');

                        $row = $index + 2;
                        $sheet->setCellValue('A'.$row, ($index+1));
                        $sheet->setCellValue('B'.$row, $factory_id);
                        $sheet->setCellValue('C'.$row, 'FM');
                        $sheet->setCellValue('D'.$row, $efaktur->kode_jenis);
                        $sheet->setCellValue('E'.$row, $efaktur->fg_pengganti);
                        $sheet->setCellValue('F'.$row, $efaktur->nomor_faktur);
                        $sheet->setCellValue('G'.$row, $efaktur->bulan_finance);
                        $sheet->setCellValue('H'.$row, $tanggal_scan->year);
                        $sheet->setCellValue('I'.$row, $tanggal_pajak);
                        $sheet->setCellValue('J'.$row, $efaktur->npwp_penjual);
                        $sheet->setCellValue('K'.$row, $efaktur->nama_penjual);
                        $sheet->setCellValue('L'.$row, $efaktur->alamat_penjual);
                        $sheet->setCellValue('M'.$row, $efaktur->jumlah_dpp);
                        $sheet->setCellValue('N'.$row, $efaktur->jumlah_ppn);
                        $sheet->setCellValue('O'.$row, $efaktur->jumlah_ppnbm);
                        $sheet->setCellValue('P'.$row, $creditable);
                        $sheet->setCellValue('Q'.$row, $efaktur->referensi);
                        $index++;
                    }
                });
                $excel->setActiveSheetIndex(0);

            })->download('xlsx');
        //}
    }


//===================================================== useless ========================================
    public function uploadFaktur()
    {
        $factory   = auth::user()->factory_id;


        return view('faktur.upload_faktur',compact('factory'));
    }


    public function uploadFakturHeader(Request $request)
    {
        //return view('errors.503');
        $array = array();
        if($request->hasFile('upload_file_header'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file_header' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file_header')->getRealPath();
            $data = Excel::selectSheets('Sheet1')->load($path,function($render){})->get();

            // dd($data);

            // dd($data);
            if(!empty($data) && $data->count())
            {
                try
                {
                    DB::beginTransaction();
                //return response()->json($data);
                    $i = 1;
                    foreach ($data as $key => $value)
                    {
                        $id           = null;
                        $nomor_faktur = $value->nomor_faktur;

                        $cek_data = Faktur::where([
                            'nomor_faktur' => $nomor_faktur,
                            'deleted_at'   => null,
                        ])
                        ->exists();


                        if($nomor_faktur == null){

                        }
                        else{
                            if(!$cek_data){
                                $faktur_header = Faktur::Create([
                                    'kode_jenis'             => $value->kode_jenis,
                                    'fg_pengganti'           => $value->fg_pengganti,
                                    'nomor_faktur'           => $value->nomor_faktur,
                                    'tanggal_faktur'         => $value->tanggal_faktur,
                                    'npwp_penjual'           => $value->npwp_penjual,
                                    'nama_penjual'           => $value->nama_penjual,
                                    'alamat_penjual'         => $value->alamat_penjual,
                                    'npwp_lawan_transaksi'   => $value->npwp_lawan_transaksi,
                                    'nama_lawan_transaksi'   => $value->nama_lawan_transaksi,
                                    'alamat_lawan_transaksi' => $value->alamat_lawan_transaksi,
                                    'jumlah_dpp'             => $value->jumlah_dpp,
                                    'jumlah_ppn'             => $value->jumlah_ppn,
                                    'jumlah_ppnbm'           => $value->jumlah_ppnbm,
                                    'status_approval'        => $value->status_approval,
                                    'status_faktur'          => $value->status_faktur,
                                    'referensi'              => $value->referensi,
                                    'bulan_finance'          => $value->bulan_finance,
                                    'factory'                => $value->factory,
                                    'created_at'             => $value->created_at,
                                    'created_by'             => $value->created_by,
                                ]);


                                $carbon = Carbon::now();
                                $obj    = new stdClass();

                                $obj->no           = $i;
                                $obj->nomor_faktur = $nomor_faktur;
                                // dd();
                                $obj->is_error     = false;
                                $obj->status       = 'success';

                                $array[]           = $obj;
                                $i++;
                            }
                            else{
                                $carbon = Carbon::now();
                                $obj    = new stdClass();

                                $obj->no           = $i;
                                $obj->nomor_faktur = 'data not found';
                                // dd();
                                $obj->is_error     = true;
                                $obj->status       = 'error';

                                $array[]           = $obj;
                                $i++;
                            }
                        }

                    }

                    DB::commit();
                } catch (Exception $e)
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }


        }

        return response()->json($array,'200');
        /*die();
        usort($array, function($a, $b) {
            return strcmp($a->order_by,$b->order_by);
        });
        return response()->json($array,'200');*/
    }

    public function uploadFakturDetail(Request $request)
    {
        //return view('errors.503');
        $array = array();
        if($request->hasFile('upload_file_detail'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file_detail' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file_detail')->getRealPath();
            $data = Excel::selectSheets('Sheet1')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try
                {
                    DB::beginTransaction();
                //return response()->json($data);
                    $i = 1;
                    foreach ($data as $key => $value)
                    {
                        $id           = null;
                        $nomor_faktur = $value->nomor_faktur;
                        // $nama         = $value->nama;

                        if($nomor_faktur == null){

                        }else{
                            $cek_data = Faktur::where([
                                'nomor_faktur' => $nomor_faktur,
                                // 'nama'         => $nama,
                                'deleted_at'   => null,
                            ])
                            ->exists();

                            if($cek_data){

                                $header = Faktur::where([
                                    'nomor_faktur' => $nomor_faktur,
                                    'deleted_at'   => null,
                                ])
                                ->first();

                                $faktur_header = DetailFaktur::Create([
                                    'faktur_id'     => $header->id,
                                    'nama'          => $value->nama,
                                    'harga_satuan'  => $value->harga_satuan,
                                    'jumlah_barang' => $value->jumlah_barang,
                                    'harga_total'   => $value->harga_total,
                                    'diskon'        => $value->diskon,
                                    'dpp'           => $value->dpp,
                                    'ppn'           => $value->ppn,
                                    'tarif_ppnbm'   => $value->tarif_ppnbm,
                                    'ppnbm'         => $value->ppnbm,
                                    'created_at'    => $value->create_date,
                                ]);


                                $carbon = Carbon::now();
                                $obj    = new stdClass();

                                $obj->no           = $i;
                                $obj->nomor_faktur = $nomor_faktur;
                                // dd();
                                $obj->is_error     = false;
                                $obj->status       = 'success';

                                $array[]           = $obj;
                                $i++;
                            }
                            else{
                                $carbon = Carbon::now();
                                $obj    = new stdClass();

                                $obj->no           = $i;
                                $obj->nomor_faktur = 'data not found';
                                // dd();
                                $obj->is_error     = true;
                                $obj->status       = 'error';

                                $array[]           = $obj;
                                $i++;
                            }
                        }

                    }

                    DB::commit();
                } catch (Exception $e)
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }


        }

        return response()->json($array,'200');
        /*die();
        usort($array, function($a, $b) {
            return strcmp($a->order_by,$b->order_by);
        });
        return response()->json($array,'200');*/
    }

    //=================================================unused funciton=======================================================
    public function lock()
    {
        // $issue = IssueCategory::pluck('name', 'id')->all();
        return view('faktur.lock');
    }

    public function lockFaktur(Request $request)
    {
        $this->validate($request, [
            'bulan'                 => 'required',
        ]);

        $carbon = Carbon::now();

        $bulan = $request->bulan;


        $query = Faktur::where([['bulan_finance', $bulan],['closed_at',null],['closed_by',null]]);

        if($query->exists() == null){

            return response()->json(['message' => 'Tidak ada faktur di bulan '.$nama_bulan.' yang masih open!'], 422);

        }
        else{

            $nama_bulan = Carbon::parse($bulan)->format('M');
            // foreach ($query->get() as $key => $value) {
            //     $efaktur = Faktur::find($value->id);
            //     $efaktur->closed_at = $carbon;
            //     $efaktur->closed_by = Auth::user()->nik;
            //     $efaktur->save();
            // }

            return response()->json(['message' => 'Semua faktur di bulan '.$nama_bulan.' berhasil di closed!'], 200);


            // return response()->json(['bulan' => $bulan ,'success',200]);
        }
    }



}

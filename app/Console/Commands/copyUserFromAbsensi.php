<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use DB;

use App\Models\User;
use App\Models\Role;
use Illuminate\Console\Command;

class copyUserFromAbsensi extends Command
{
    protected $signature = 'copyDataUser:insert';
    protected $description = 'Copy Data User From Absensi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('START EVENT AT '.carbon::now());
        $this->InsertUserFromAbsensi();
        $this->info('END EVENT AT '.carbon::now());

    }

    private function InsertUserFromAbsensi()
    {
        $users   = DB::connection('absence_aoi')->select(db::raw("
        SELECT *
        FROM public.adt_bbigroup_emp
        where department like ('Finance%')
        or department like ('FINANCE%')
        order by department
        "));

        //dd($users);

        try
        {
            DB::beginTransaction();
            $no = 1;

            $roles  = Role::Select('id')->where('name','finance')->get();

            foreach( $users as $key => $user)
            {
                $name           = $user->name;
                $nik            = $user->nik;
                $warehouse_name = $user->factory;

                if($warehouse_name == 'AOI2')
                {
                    $warehouse_id  = '1000002';
                }
                else if($warehouse_name == 'AOI1')
                {
                    $warehouse_id  = '1000001';
                }else {
                    $warehouse_id  = '1000003';
                }

                $user = User::FirstOrCreate([
                    'name'              => $name,
                    'nik'               => $nik,
                    'warehouse_id'      => $warehouse_id,
                    'email'             => 'user'.$no.'@'.$warehouse_id.'.co.id',
                    'email_verified_at' => carbon::now(),
                    'password'          => bcrypt('password1'),
                    ]);

                $user->attachRoles($roles);
                $no++;
            }
            DB::commit();
        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }


    }
}

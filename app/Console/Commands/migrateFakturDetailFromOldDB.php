<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use DB;

use App\Models\Faktur;
use App\Models\DetailFaktur;
use App\Models\User;
use App\Models\Role;
use Illuminate\Console\Command;


class migrateFakturDetailFromOldDB extends Command
{
    protected $signature = 'migrateFakturDetailFromOldDB:migrate';
    protected $description = 'Migrate Detail Data From Old Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('START EVENT AT '.carbon::now());
        $this->UploadDetailFaktur();
        $this->info('END EVENT AT '.carbon::now());

    }


    private function UploadDetailFaktur()
    {
        $data  = DB::connection('faktur_aoi1')
        ->table('detail_transaksi')
        ->get();

        if(!empty($data) && $data->count())
        {
            try
            {
                DB::beginTransaction();

                foreach ($data as $key => $value)
                {
                    $nomor_faktur = $value->nomorFaktur;
                    // $nama         = $value->nama;

                    if($nomor_faktur == null){

                    }else{
                        $cek_data = Faktur::where([
                            'nomor_faktur' => $nomor_faktur,
                            // 'nama'         => $nama,
                            'deleted_at'   => null,
                        ])
                        ->exists();

                        if($cek_data){

                            $header = Faktur::where([
                                'nomor_faktur' => $nomor_faktur,
                                'deleted_at'   => null,
                            ])
                            ->first();

                            $faktur_detail = DetailFaktur::Create([
                                'faktur_id'     => $header->id,
                                'nama'          => $value->nama,
                                'harga_satuan'  => $value->hargaSatuan,
                                'jumlah_barang' => $value->jumlahBarang,
                                'harga_total'   => $value->hargaTotal,
                                'diskon'        => $value->diskon,
                                'dpp'           => $value->dpp,
                                'ppn'           => $value->ppn,
                                'tarif_ppnbm'   => $value->tarifppnbm,
                                'ppnbm'         => $value->ppnbm,
                                'created_at'    => $value->create_date,
                            ]);
                        }
                        else{
                        }
                    }

                }

                DB::commit();
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

    }
}

<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use DB;

use App\Models\Faktur;
use App\Models\User;
use App\Models\Role;
use Illuminate\Console\Command;


class migrateFakturFromOldDB extends Command
{
    protected $signature = 'migrateFakturFromOldDB:migrate';
    protected $description = 'Migrate Data From Old Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('START EVENT AT '.carbon::now());
        $this->UploadHeaderFaktur();
        $this->info('END EVENT AT '.carbon::now());

    }


    private function UploadHeaderFaktur()
    {
        $data  = DB::connection('faktur_aoi1')
        ->table('faktur_pajak')
        ->where(db::raw('date(create_date)'),'>=','2017-01-01')
        ->get();

        if(!empty($data) && $data->count())
        {
            try
            {
                DB::beginTransaction();

                foreach ($data as $key => $value)
                {
                    $nomor_faktur = $value->nomorFaktur;

                    $cek_data = Faktur::where([
                        'nomor_faktur' => $nomor_faktur,
                        'deleted_at'   => null,
                    ])
                    ->exists();


                    if($nomor_faktur == null){

                    }
                    else{
                        if(!$cek_data){


                            $tanggal_faktur = Carbon::createFromFormat('d/m/Y',$value->tanggalFaktur)->format('Y-m-d');

                            $faktur_header = Faktur::Create([
                                'kode_jenis'             => $value->kdJenisTransaksi,
                                'fg_pengganti'           => $value->fgPengganti,
                                'nomor_faktur'           => $value->nomorFaktur,
                                'tanggal_faktur'         => $tanggal_faktur,
                                'npwp_penjual'           => $value->npwpPenjual,
                                'nama_penjual'           => $value->namaPenjual,
                                'alamat_penjual'         => $value->alamatPenjual,
                                'npwp_lawan_transaksi'   => $value->npwpLawanTransaksi,
                                'nama_lawan_transaksi'   => $value->namaLawanTransaksi,
                                'alamat_lawan_transaksi' => $value->alamatLawanTransaksi,
                                'jumlah_dpp'             => $value->jumlahDpp,
                                'jumlah_ppn'             => $value->jumlahPpn,
                                'jumlah_ppnbm'           => $value->jumlahPpnBm,
                                'status_approval'        => $value->statusApproval,
                                'status_faktur'          => $value->statusFaktur,
                                'referensi'              => $value->referensi,
                                'bulan_finance'          => $value->masaPajak,
                                'factory'                => '1',
                                'created_at'             => $value->create_date,
                                'created_by'             => '11111111',
                            ]);
                        }
                    }

                }

                DB::commit();
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

    }
}
